package Presentacion;

import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import Negocios.aplicacion;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class elegirPaisUsuario extends JDialog {
	private static final long serialVersionUID = 1L;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			elegirPaisUsuario dialog = new elegirPaisUsuario();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public elegirPaisUsuario() {
		setTitle("Elegir el pais para jugar");
		setBounds(100, 100, 262, 246);
		setResizable(false);
		getContentPane().setLayout(null);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBounds(0, 169, 246, 33);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));

			argentina();
			bolivia();
			brasil();
			chile();
			colombia();
			ecuador();
			guyana();
			paraguay();
			peru();
			surinam();
			uruguay();
			venezuela();

			getContentPane().add(buttonPane);
			{
				JButton btnJugar = new JButton("Jugar");
				btnJugar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
						agregarJugador();
						aplicacion.cargarPreguntas();
						comenzarJuego();
					}
				});
				buttonPane.add(btnJugar);
				getRootPane().setDefaultButton(btnJugar);
			}
			{
				JButton btnCancelar = new JButton("Cancelar");
				btnCancelar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						setVisible(false);
					}
				});
				buttonPane.add(btnCancelar);
			}
		}

	}

	private void venezuela() {
		JRadioButton rbtnVenezuela = new JRadioButton("Venezuela");
		rbtnVenezuela.setActionCommand(rbtnVenezuela.getText());
		buttonGroup.add(rbtnVenezuela);
		rbtnVenezuela.setBounds(146, 139, 87, 23);
		getContentPane().add(rbtnVenezuela);
		JLabel lblVenezuela = new JLabel(new ImageIcon("img/banderas/ve.png"));
		lblVenezuela.setBounds(126, 139, 16, 23);
		getContentPane().add(lblVenezuela);
	}

	private void uruguay() {
		JRadioButton rbtnUruguay = new JRadioButton("Uruguay");
		rbtnUruguay.setActionCommand(rbtnUruguay.getText());
		buttonGroup.add(rbtnUruguay);
		rbtnUruguay.setBounds(146, 113, 87, 23);
		getContentPane().add(rbtnUruguay);
		JLabel lblUruguay = new JLabel(new ImageIcon("img/banderas/uy.png"));
		lblUruguay.setBounds(126, 113, 16, 23);
		getContentPane().add(lblUruguay);
	}

	private void surinam() {
		JRadioButton rbtnSuriman = new JRadioButton("Surinam");
		rbtnSuriman.setActionCommand(rbtnSuriman.getText());
		buttonGroup.add(rbtnSuriman);
		rbtnSuriman.setBounds(146, 87, 87, 23);
		getContentPane().add(rbtnSuriman);
		JLabel lblSurinam = new JLabel(new ImageIcon("img/banderas/sr.png"));
		lblSurinam.setBounds(126, 87, 16, 23);
		getContentPane().add(lblSurinam);
	}

	private void peru() {
		JRadioButton rbtnPeru = new JRadioButton("Per�");
		rbtnPeru.setActionCommand(rbtnPeru.getText());
		buttonGroup.add(rbtnPeru);
		rbtnPeru.setBounds(146, 61, 87, 23);
		getContentPane().add(rbtnPeru);

		JLabel lblPeru = new JLabel(new ImageIcon("img/banderas/pe.png"));
		lblPeru.setBounds(126, 61, 16, 23);
		getContentPane().add(lblPeru);
	}

	private void paraguay() {
		JRadioButton rbtnParaguay = new JRadioButton("Paraguay");
		rbtnParaguay.setActionCommand(rbtnParaguay.getText());
		buttonGroup.add(rbtnParaguay);
		rbtnParaguay.setBounds(146, 34, 87, 23);
		getContentPane().add(rbtnParaguay);
		JLabel lblParaguay = new JLabel(new ImageIcon("img/banderas/pa.png"));
		lblParaguay.setBounds(126, 34, 16, 23);
		getContentPane().add(lblParaguay);
	}

	private void guyana() {
		JRadioButton rbtnGuyana = new JRadioButton("Guyana");
		rbtnGuyana.setActionCommand(rbtnGuyana.getText());
		buttonGroup.add(rbtnGuyana);
		rbtnGuyana.setBounds(146, 7, 87, 23);
		getContentPane().add(rbtnGuyana);
		JLabel lblGuyana = new JLabel(new ImageIcon("img/banderas/gy.png"));
		rbtnGuyana.setActionCommand(rbtnGuyana.getText());
		lblGuyana.setBounds(126, 7, 16, 23);
		getContentPane().add(lblGuyana);
	}

	private void ecuador() {
		JRadioButton rbtnEcuador = new JRadioButton("Ecuador");
		rbtnEcuador.setActionCommand(rbtnEcuador.getText());
		buttonGroup.add(rbtnEcuador);
		rbtnEcuador.setBounds(30, 139, 87, 23);
		getContentPane().add(rbtnEcuador);
		JLabel lblEcuador = new JLabel(new ImageIcon("img/banderas/ec.png"));
		lblEcuador.setBounds(10, 139, 16, 23);
		getContentPane().add(lblEcuador);
	}

	private void colombia() {
		JRadioButton rbtnColombia = new JRadioButton("Colombia");
		rbtnColombia.setActionCommand(rbtnColombia.getText());
		buttonGroup.add(rbtnColombia);
		rbtnColombia.setBounds(30, 113, 87, 23);
		getContentPane().add(rbtnColombia);
		JLabel lblColombia = new JLabel(new ImageIcon("img/banderas/co.png"));
		lblColombia.setBounds(10, 113, 16, 23);
		getContentPane().add(lblColombia);
	}

	private void chile() {
		JRadioButton rbtnChile = new JRadioButton("Chile");
		rbtnChile.setActionCommand(rbtnChile.getText());
		buttonGroup.add(rbtnChile);
		rbtnChile.setBounds(30, 87, 87, 23);
		getContentPane().add(rbtnChile);
		JLabel lblChile = new JLabel(new ImageIcon("img/banderas/ch.png"));
		lblChile.setBounds(10, 87, 16, 23);
		getContentPane().add(lblChile);
	}

	private void brasil() {
		JRadioButton rbtnBrasil = new JRadioButton("Brasil");
		rbtnBrasil.setActionCommand(rbtnBrasil.getText());
		buttonGroup.add(rbtnBrasil);
		rbtnBrasil.setBounds(30, 61, 87, 23);
		getContentPane().add(rbtnBrasil);
		JLabel lblBrasil = new JLabel(new ImageIcon("img/banderas/br.png"));
		lblBrasil.setBounds(10, 61, 16, 23);
		getContentPane().add(lblBrasil);
	}

	private void bolivia() {
		JRadioButton rbtnBolivia = new JRadioButton("Bolivia");
		rbtnBolivia.setActionCommand(rbtnBolivia.getText());
		buttonGroup.add(rbtnBolivia);
		rbtnBolivia.setBounds(30, 34, 87, 23);
		getContentPane().add(rbtnBolivia);
		JLabel lblBolivia = new JLabel(new ImageIcon("img/banderas/bo.png"));
		lblBolivia.setBounds(10, 34, 16, 23);
		getContentPane().add(lblBolivia);
	}

	private void argentina() {
		JRadioButton rbtnArgentina = new JRadioButton("Argentina", true);
		rbtnArgentina.setActionCommand(rbtnArgentina.getText());
		buttonGroup.add(rbtnArgentina);
		rbtnArgentina.setBounds(30, 7, 73, 23);
		getContentPane().add(rbtnArgentina);
		JLabel lblArgentina = new JLabel(new ImageIcon("img/banderas/ar.png"));
		lblArgentina.setBounds(10, 7, 16, 23);
		getContentPane().add(lblArgentina);
	}

	protected void agregarJugador() {
		// TODO Auto-generated method stub
		aplicacion.agregarJugador(buttonGroup.getSelection().getActionCommand()); // Obtengo
																					// mis
																					// datos
																					// primarios
																					// jugador

	}

	private void comenzarJuego() {
		// TODO Auto-generated method stub
		// Cargar preguntas antes de comenzar el juego

		new comenzarJuego().setVisible(true);

	}
}
