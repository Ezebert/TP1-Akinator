package Presentacion;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.border.TitledBorder;

import Acceso.jugadorAcceso;
import Acceso.paisAcceso;
import Acceso.preguntaAcceso;
import Datos.pregunta;
import Negocios.akinator;
import Negocios.aplicacion;

import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;

public class comenzarJuego extends JDialog {

	private static final long serialVersionUID = 1L;
	JLabel lblPreguntas = new JLabel();
	private JLabel lblArgentina;
	private JLabel lblBolivia;
	private JLabel lblBrasil;
	private JLabel lblChile;
	private JLabel lblColombia;
	private JLabel lblEcuador;
	private JLabel lblGuyana;
	private JLabel lblParaguay;
	private JLabel lblPeru;
	private JLabel lblSurinam;
	private JLabel lblUruguay;
	private JLabel lblVenezuela;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private ArrayList<pregunta> listaPregunta;
	private int index=0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {		
		try {
			comenzarJuego dialog = new comenzarJuego();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Create the dialog.
	 */
	public comenzarJuego() {
		setTitle("Comenzar Juego");
		setBounds(100, 100, 386, 289);
		getContentPane().setLayout(null);
		
		aplicacion.cargarPreguntas();
		
		//						llamdao de accesoPreguntas - llamado de la clase preg
		listaPregunta =preguntaAcceso.getPregunta();
		lblPreguntas.setText(listaPregunta.get(index).getPregunta());
		lblPreguntas.setHorizontalAlignment(SwingConstants.CENTER);
		lblPreguntas.setBounds(11, 11, 347, 14);
		getContentPane().add(lblPreguntas);

		panelDecision(); 

		panelPaises();

		JLabel lblPaisElegido = new JLabel("Elegiste el Pais: "+jugadorAcceso.getJugador().getPais(), SwingConstants.CENTER);
		lblPaisElegido.setBounds(11, 237, 347, 14);
		getContentPane().add(lblPaisElegido);
		
		JButton button = new JButton("Rendite !!");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				redirse(); 
			}

			private void redirse() {
				int opctionPanel = JOptionPane.showConfirmDialog(null,
						"�Desea volver a jugar?",
						"Jugar de nuevo",
						JOptionPane.OK_CANCEL_OPTION);
				if(opctionPanel==JOptionPane.YES_OPTION){
					setVisible(false);
				}
			}
		});
		button.setBounds(11, 186, 94, 23);
		getContentPane().add(button);
	}

	private void panelPaises() {
		JPanel panelPaises = new JPanel();
		panelPaises.setBorder(new TitledBorder(null, "Paises", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelPaises.setBounds(149, 33, 209, 205);
		getContentPane().add(panelPaises);
		panelPaises.setLayout(null);
		
		JLabel lblArgentina = new JLabel("Argentina",new ImageIcon("img/banderas/ar.png"),SwingConstants.LEFT);
		lblArgentina.setBounds(10, 22, 86, 20);
		panelPaises.add(lblArgentina);
		
		JLabel lblBolivia = new JLabel("Bolivia",new ImageIcon("img/banderas/bo.png"),SwingConstants.LEFT);
		lblBolivia.setBounds(10, 53, 86, 20);
		panelPaises.add(lblBolivia);
		
		JLabel lblBrasil = new JLabel("Brasil",new ImageIcon("img/banderas/br.png"),SwingConstants.LEFT);
		lblBrasil.setBounds(10, 84, 86, 20);
		panelPaises.add(lblBrasil);
		
		JLabel lblChile = new JLabel("Chile",new ImageIcon("img/banderas/ch.png"),SwingConstants.LEFT);
		lblChile.setBounds(10, 115, 86, 20);
		panelPaises.add(lblChile);
		
		JLabel lblColombia = new JLabel("Colombia",new ImageIcon("img/banderas/co.png"),SwingConstants.LEFT);
		lblColombia.setBounds(10, 146, 86, 20);
		panelPaises.add(lblColombia);
		
		JLabel lblEcuador = new JLabel("Ecuador",new ImageIcon("img/banderas/ec.png"),SwingConstants.LEFT);
		lblEcuador.setBounds(10, 177, 86, 20);
		panelPaises.add(lblEcuador);
		
		JLabel lblGuyana = new JLabel("Guyana",new  ImageIcon("img/banderas/gy.png"),SwingConstants.LEFT);
		lblGuyana.setBounds(106, 22, 86, 20);
		panelPaises.add(lblGuyana);
		JLabel lblParaguay = new JLabel("Paraguay",new  ImageIcon("img/banderas/pa.png"),SwingConstants.LEFT);
		//JLabel lblParaguay = new JLabel(new  ImageIcon("img/banderas/pa.png"));
		lblParaguay.setBounds(106, 53, 86, 20);
		panelPaises.add(lblParaguay);
		
		JLabel lblPeru = new JLabel("Peru",new  ImageIcon("img/banderas/pe.png"),SwingConstants.LEFT);
		lblPeru.setBounds(106, 84, 86, 20);
		panelPaises.add(lblPeru);
		
		JLabel lblSurinam = new JLabel("Surinam",new  ImageIcon("img/banderas/sr.png"),SwingConstants.LEFT);
		lblSurinam.setBounds(106, 115, 86, 20);
		panelPaises.add(lblSurinam);
		
		JLabel lblUruguay = new JLabel("Uruguay",new  ImageIcon("img/banderas/uy.png"),SwingConstants.LEFT);
		lblUruguay.setBounds(106, 146, 86, 20);
		panelPaises.add(lblUruguay);
		
		JLabel lblVenezuela = new JLabel("Venezuela",new  ImageIcon("img/banderas/ve.png"),SwingConstants.LEFT);
		lblVenezuela.setBounds(106, 177, 86, 20);
		panelPaises.add(lblVenezuela);
	}

	private void panelDecision() {
		JPanel panelDesicion = new JPanel();
		panelDesicion.setBorder(new TitledBorder(null, "Desicion", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelDesicion.setBounds(11, 33, 128, 115);
		getContentPane().add(panelDesicion);
		panelDesicion.setLayout(null);
		
		JRadioButton rbtnSi = new JRadioButton("Si");
		buttonGroup.add(rbtnSi);
		rbtnSi.setBounds(6, 25, 48, 23);
		panelDesicion.add(rbtnSi);
		
		JRadioButton rbtnNo = new JRadioButton("No");
		buttonGroup.add(rbtnNo);
		rbtnNo.setBounds(6, 51, 48, 23);
		panelDesicion.add(rbtnNo);
		
		JRadioButton rdbtnSinSeleccion = new JRadioButton("",true);
		rdbtnSinSeleccion.setVisible(false);
		buttonGroup.add(rdbtnSinSeleccion);
		panelDesicion.add(rdbtnSinSeleccion);
		
		JButton btnSiguiente = new JButton("Siguiente");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!rdbtnSinSeleccion.isSelected()){//tiene que estar seleccionado el radio button SI o NO
					int indice = preguntaAcceso.posicionActual(lblPreguntas.getText());
					akinator.set_respuesta(indice, rbtnSi.isSelected());
					cambiarNombreLbl(indice+1);
					rdbtnSinSeleccion.setSelected(true);	
				}	
			}
		});
		btnSiguiente.setBounds(6, 81, 94, 23);
		panelDesicion.add(btnSiguiente);
	}

	private void cambiarNombreLbl(int indice) {

		if (indice < preguntaAcceso.getPregunta().size()) {
			lblPreguntas.setText(listaPregunta.get(indice).getPregunta());
		} else if (indice == preguntaAcceso.getPregunta().size()){
			informePaisEncontrado();
			volverJugar();
		}

	}

	private void informePaisEncontrado() {
		String paisEncontrado = akinator.analizarResultado(paisAcceso.getPaises());
		int opctionPane = JOptionPane.showConfirmDialog(null, 
				"El Pais encontrado es : "+paisEncontrado,
				"Pais encotrado",
				JOptionPane.YES_NO_OPTION
				);
		if(opctionPane==JOptionPane.YES_OPTION && paisEncontrado.equals(jugadorAcceso.getJugador().getPais())){
			JOptionPane.showMessageDialog(null, "Mejor suerte para la siguiente.. continuemos con el desafio");
		}
		else if(opctionPane==JOptionPane.NO_OPTION && !paisEncontrado.equals(jugadorAcceso.getJugador().getPais())){
			JOptionPane.showMessageDialog(null, "Me GANASTE esta partida ... Eres bueno");
		}
		else {JOptionPane.showMessageDialog(null, "Se olfatea el olor a la mentira ");}
	}

	private void volverJugar() {
		int opctionPanel = JOptionPane.showConfirmDialog(null,
				"�Desea volver a jugar?",
				"Jugar de nuevo",
				JOptionPane.OK_CANCEL_OPTION);
		if(opctionPanel==JOptionPane.YES_OPTION){
			setVisible(false);
			new elegirPaisUsuario().setVisible(true);
		} 
		else setVisible(false);
	}
}
