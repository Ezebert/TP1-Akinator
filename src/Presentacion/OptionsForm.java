package Presentacion;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;
import javax.swing.ImageIcon;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;


public class OptionsForm extends JPanel{
	//---------------------------------------------------
	private static final long serialVersionUID = 1L;
	private Image imagen;
	private String urlImagen;
	//---------------------------------------------------
	public OptionsForm(String url) {		
		urlImagen = url;
		setLayout(null);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(10, 11, 62, 21);
		add(menuBar);
		
		JMenu mnAkinator = new JMenu("Akinator");
		menuBar.add(mnAkinator);
		
		JMenuItem mntmJugar = new JMenuItem("Jugar");
		mntmJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				jugar();
				
			}
		});
		mnAkinator.add(mntmJugar);
		
		JMenuItem mntmAyuda = new JMenuItem("Ayuda");
		mntmAyuda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ayuda().setVisible(true);
			}
		});
		mnAkinator.add(mntmAyuda);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				salir();
			}

			private void salir() {
				int opctionPanel = JOptionPane.showConfirmDialog(null,
						"�Desea salir de akinator?",
						"Salir",
						JOptionPane.OK_CANCEL_OPTION);
				if(opctionPanel==JOptionPane.YES_OPTION)
					System.exit(1);
			}
		});
		mnAkinator.add(mntmSalir);
		
	}


	protected void ayuda() {
		// TODO Auto-generated method stub
		new ayuda().setVisible(true);
		
	}


	protected void jugar() {
		// TODO Auto-generated method stub
		
		new elegirPaisUsuario().setVisible(true);		
		
	}


	//---------------------------------------------------
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public void paint(Graphics g)
	{
		imagen = new ImageIcon(urlImagen).getImage();
		g.drawImage(imagen, 0, 0, this.getWidth(), this.getHeight(), null);
		setOpaque(false);
		super.paint(g);
	}
}
