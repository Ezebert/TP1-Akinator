package Presentacion;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ayuda extends JDialog {

	private static final long serialVersionUID = 1L;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ayuda dialog = new ayuda();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ayuda() {
		setTitle("Ayuda");
		setBounds(100, 100, 720, 560);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel panelBotones = new JPanel();
			panelBotones.setLayout(new FlowLayout(FlowLayout.RIGHT));
			JLabel lblImagen = new JLabel("");
			getContentPane().add(panelBotones, BorderLayout.SOUTH);
			{
				JButton btnRegresar = new JButton("Regresar");
				btnRegresar.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
					}
				});
				JButton btnVerMapa = new JButton("Ver Mapa");
				btnVerMapa.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						verImagen("img/SurAmerica.png",lblImagen);
					}

				});
				panelBotones.add(btnVerMapa);

				JButton btnVerRespuestas = new JButton("Ver Respuestas");
				btnVerRespuestas.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						verImagen("img/respuestas.png",lblImagen);
					}
				});
				panelBotones.add(btnVerRespuestas);
				panelBotones.add(btnRegresar);
			}
		}

	}

	protected void verImagen(String url, JLabel lblImagen) {
		// TODO Auto-generated method stub
		
		lblImagen.setLayout(new FlowLayout(FlowLayout.CENTER));
		getContentPane().add(lblImagen, BorderLayout.CENTER);
		lblImagen.setIcon(new ImageIcon(url));
		

	}

}
