package Presentacion;


import javax.swing.JFrame;
import javax.swing.WindowConstants;



public class mainForm {
	public static void main(String[] args){
		JFrame frame = new JFrame();;
		frame.setTitle("Akinator - AguilasNegras ");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setSize(640,480);
		frame.setResizable(false);
		OptionsForm optionsForm = new OptionsForm("img/AguilasNegras.jpeg");
		frame.getContentPane().add(optionsForm);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

}
