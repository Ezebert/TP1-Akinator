package Negocios;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import Acceso.jugadorAcceso;
import Acceso.paisAcceso;
import Acceso.preguntaAcceso;
import Datos.*;

public class aplicacion {

	public static void agregarPaises(String url) {
		// Agrego todos los Jugadores de la instancia elegida
		ArrayList<pais> paisJson = archivo.leerJson(url);
		paisAcceso.setPaises(url, paisJson);
	}

	/**
	 * @param jugadorPais:
	 *            Nombre del pais que elegio el jugador
	 */
	public static void agregarJugador(String jugadorPais) {
		// TODO Auto-generated method stub
		jugador player = jugadorAcceso.getJugador(jugadorPais);
		if (player == null)
			JOptionPane.showMessageDialog(null, "No se encontraron jugador cargado....");
		else
			jugadorAcceso.setJugador(player);
		// JOptionPane.showMessageDialog(null,
		// "Usted elegio el pais: " + jugadorAcceso.getJugador().getPais());
	}

	public static void cargarPreguntas() {
		// TODO Auto-generated method stub
		ArrayList<pregunta> preguntas = preguntaAcceso.getPreguntasBasicas();
		if (preguntas == null)
			JOptionPane.showMessageDialog(null, "No se encontraron preguntas cargado....");
		else
			preguntaAcceso.setPreguntas(preguntas);
	}
}
