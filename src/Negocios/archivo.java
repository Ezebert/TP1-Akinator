package Negocios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import Datos.pais;

public class archivo {
	// ================================================================================

	public static ArrayList<pais> leerJson(String url) {
		// TODO Auto-generated method stub
		ArrayList<pais> listaPais = null;

		if (!new File(url).exists())
			return listaPais;
		try {
			BufferedReader br = new BufferedReader(new FileReader(url));
			Type tipoJugador = new TypeToken<List<pais>>() {
			}.getType();
			listaPais = new Gson().fromJson(br, tipoJugador);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return listaPais;

	}

	public static void escribirJson(String url, ArrayList<pais> listaPais) {
		// TODO Auto-generated method stub
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(listaPais);
		try {
			FileWriter writer = new FileWriter(url);
			writer.write(json);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
