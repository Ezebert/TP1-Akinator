package Acceso;

import javax.swing.JOptionPane;

import Datos.jugador;

public class jugadorAcceso {
	private static jugador _jugador = null;

	public static void setJugador(jugador player) {
		// TODO Auto-generated method stub
		_jugador = player;
	}

	public static jugador getJugador(String jugadorPais) {
		// TODO Auto-generated method stub
		_jugador = new jugador(jugadorPais);
		return _jugador;
	}

	public static jugador getJugador() {
		if (_jugador == null)
			JOptionPane.showMessageDialog(null, "No se encontraron jugador cargado....");
		return _jugador;
	}

}
