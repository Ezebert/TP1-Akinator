
package Acceso;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import Datos.pregunta;

public class preguntaAcceso {
	private static ArrayList<pregunta> _preguntas = new ArrayList<pregunta>();

	public static ArrayList<pregunta> getPregunta() {
		// TODO Auto-generated method stub
		if (_preguntas == null) {
			JOptionPane.showMessageDialog(null, "No se encontraron preguntas cargado....");
		}
		return _preguntas;
	}

	public static void setPreguntas(ArrayList<pregunta> preguntas) {
		// TODO Auto-generated method stub
		_preguntas = preguntas;

	}

	public static ArrayList<pregunta> getPreguntasBasicas() {
		// TODO Auto-generated method stub
		ArrayList<pregunta> preguntasBasicas = new ArrayList<pregunta>();
		preguntasBasicas.add(new pregunta("0: �Tiene mar?"));
		preguntasBasicas.add(new pregunta("1: �Tiene estrella en la bandera?"));
		preguntasBasicas.add(new pregunta("2: �Contine un sol ?"));
		preguntasBasicas.add(new pregunta("3: �Tiene Escudo?"));
		preguntasBasicas.add(new pregunta("4: �Su bandera tiene el  color Verde?"));
		preguntasBasicas.add(new pregunta("5: �Su bandera tiene el tiene Color Azul/Celeste?"));
		preguntasBasicas.add(new pregunta("6: �Su bandera tiene el tiene color Amarrilo?"));
		preguntasBasicas.add(new pregunta("7: �Su bandera tiene el  tiene color rojo?"));
		preguntasBasicas.add(new pregunta("8: �Su bandera tiene el tiene color Blanco?"));

		return preguntasBasicas;
	}

	public static int posicionActual(String text) {
		// TODO Auto-generated method stub
		int i = 0;
		for (pregunta pregunta : _preguntas) {
			if (pregunta.getPregunta().equals(text))
				break;
			i++;
		}
		return i;
	}
}
