package Acceso;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import Datos.pais;
import Negocios.archivo;

public class paisAcceso {

	public static void main(String[] args) {
		System.out.println(getPaises());
	}
	public static void setPaises(String url, ArrayList<pais> paises) {
		// TODO Auto-generated method stub
		archivo.escribirJson(url, paises);
	}

	public static ArrayList<pais> getPaises() {
		// TODO Auto-generated method stub
		ArrayList<pais> paises = archivo.leerJson("Pais.json");
		if (paises == null)
			JOptionPane.showMessageDialog(null, "No se encontraron paises cargado....");
		return paises;
	}

}
