package Datos;

public class jugador {
	private String nombre;
	private String pais;

	public jugador(String jugadorPais) {
		this.nombre = "Player ";
		this.pais = jugadorPais;

	}

	public String getPais() {
		return pais;
	}

	@Override
	public String toString() {
		return "jugador [nombre=" + nombre + ", pais=" + pais + "]";
	}

}
