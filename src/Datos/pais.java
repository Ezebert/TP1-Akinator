package Datos;

import java.util.ArrayList;

public class pais {
	private String nombre;
	ArrayList<Boolean> caracteristicas;

	public pais() {
		nombre = "Pais";
		caracteristicas = new ArrayList<Boolean>();
		for (int i = 0; i < 9; i++) {
			caracteristicas.add(false);
		}

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public ArrayList<Boolean> getCaracteristicas() {
		return caracteristicas;
	}

	public void setCaracteristicas(ArrayList<Boolean> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

	@Override
	public String toString() {
		return nombre + " " + caracteristicas + "\n";
	}

}
